﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class ScrollingBackground : MonoBehaviour
{

    public bool ScrollOn = false;
    public float bgSpeed = 0;
    
    private Renderer _bgRend;
    
    #region MonoBehaviours
    void Start()
    {
        _bgRend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ScrollOn)
        {
            _bgRend.material.mainTextureOffset += new Vector2(bgSpeed * Time.deltaTime, 0f);
        }
    }
    #endregion
}
