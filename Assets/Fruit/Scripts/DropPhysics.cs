﻿
using UnityEngine;

namespace FrootPoot
{
/* This class simulates a very crude gravity and rigidbody physics, without using forces or rigidbodies. 
It has lots of magic numbers, todo: make easier to edit, with a control panel with sliders and continue polishing feel.
*/

    public class DropPhysics : MonoBehaviour
    {
        public bool Falling { get; private set; }

        private float verticalSpeed;
        private float lateralSpeed;
        private float maxSpeed = 2f;
        private float dropStop;

        private Fruit _fruit;

        // Start is called before the first frame update
        void Start()
        {
            _fruit = GetComponent<Fruit>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!Falling) return;

            verticalSpeed -= Time.deltaTime * 0.5f; //gravity pulls every frame
            verticalSpeed = Mathf.Max(-0.2f, verticalSpeed);

            lateralSpeed *= (1 - Time.deltaTime * 2.5f); //desacceleration towards 0;


            if (transform.position.y <= dropStop)
            {
                Falling = false;
                transform.position =
                    new Vector3(transform.position.x, dropStop, dropStop * 10); //adopt a new Z-value based on y-value
            }

            float targetX = transform.position.x + lateralSpeed;
            float targetY = transform.position.y + verticalSpeed;

            transform.position = new Vector3(targetX, targetY, transform.position.z);

        }

        public void Activate()
        {
            lateralSpeed = _fruit.mouseDragDelta.x * 0.05f;
            lateralSpeed = Mathf.Clamp(lateralSpeed, -0.3f, 0.3f);

            verticalSpeed = _fruit.mouseDragDelta.y * 0.05f;
            verticalSpeed = Mathf.Clamp(verticalSpeed, -0.1f, 0.3f);

            dropStop = _fruit.parentTree.GetDropStopLocation();
            Falling = true;
        }

        public void Deactivate()
        {
            Falling = false;

        }
    }
}
