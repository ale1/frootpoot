﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : MonoBehaviour
{

    [Header("speed at which eyeball will track target")]
    public float speed = 1;
    [Tooltip("point to the transform of the parent container of the eye pupil.  this is the part that actually rotates towards target")]
    public Transform rotatingPart;
    [Tooltip("eyeball will follow this target transform")]
    public Transform target;
    [Tooltip("eyeball will follow target + offset")]
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        #if UNITY_EDITOR
        if(rotatingPart == null)
            Debug.LogError("ooopsie, did you forget to set this ref?");
        #endif
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;

        Vector3 dir = (target.position + offset) - transform.position;
        float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }

    public void TrackOneFrame(Vector3 pos)
    {
        Vector3 dir = pos - transform.position;
        float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
