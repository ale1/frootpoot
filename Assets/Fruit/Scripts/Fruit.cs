﻿using System;
using System.Collections;
using UnityEngine;
using static Mouth; // synctatic sugar;


namespace FrootPoot
{

    [RequireComponent(typeof(AudioSource), typeof(Animator))]
    public class Fruit : MonoBehaviour
    {

        //Fruit Types
        public enum Flavor
        {
            APPLE,
            GRAPE,
            MELON
        }

        public Flavor flavor;


        //Fruit Parts
        private Eye[] eyes;
        public Mouth mouth { get; private set; }
        public AudioSource audio { get; private set; }
        public Animator Animator { get; private set; }


        //Physics
        public DropPhysics physics { get; private set; }

        public Vector3
            mouseDragDelta
        {
            get;
            private set;
        } //measures speed at which dragging is ocurring betweeen frames. simulates "force" when GO is dropped.

        private Vector3 lastMousePos;
        private float _gameObjectZ;

        //VFX
        public GameObject HitVFX;
        public Transform vfxSpawnpoint;

        //Tree Interaction
        private AttachPoint _attachPoint;
        [HideInInspector] public Tree parentTree;

        private bool _disabledInputs;

        private FruitState _currentState;
        /*  >>>FRUIT STATES
         *      FruitStateAttached = is hanging from tree branch
         *      FruitStateDragging = is being dragged by the player
         *      FruitStateFalling = has been dropped and is affected by drop physics
         *      FruitStateWalking = is on the ground walking between boundries
         *      FruitStateEscaping = the fruit are preparing their escape...
         *      
         */


        // Start is called before the first frame update
        void Awake()
        {
            audio = GetComponent<AudioSource>();
            eyes = GetComponentsInChildren<Eye>();
            mouth = GetComponentInChildren<Mouth>();
            Animator = GetComponent<Animator>();
            physics = GetComponent<DropPhysics>();
        }

        void Start()
        {

        }

        public void SetState(FruitState newState)
        {
            if (_currentState != null && _currentState.GetType() == newState.GetType())
            {
                Debug.LogError("trying to enter state when already in it");
                return;
            }

            if (_currentState != null)
                _currentState.OnStateExit();

            _disabledInputs = false;
            _currentState = newState;
            gameObject.name = String.Concat("Fruit in state: ", _currentState.GetType().Name);

            _currentState.OnStateEnter();
        }

        void Update()
        {
            if (_currentState != null)
                _currentState.OnUpdate();
        }

        #region MouseHandlers

        public void OnMouseDown()
        {
            if (_disabledInputs) return;
            _currentState.onMouseDown();
            _gameObjectZ = gameObject.transform.position.z;
        }

        public void OnMouseUp()
        {
            _currentState.onMouseUp();
        }

        public void OnMouseEnter()
        {
            _currentState.onMouseEnter();
        }

        public void OnMouseExit()
        {
            _currentState.onMouseExit();
        }

        public void OnMouseDrag()
        {
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _gameObjectZ);
            mouseDragDelta = mousePos - lastMousePos;
            lastMousePos = mousePos;
        }

        #endregion


        #region CollisionDetection

        public void OnTriggerEnter2D(Collider2D other)
        {
            _currentState.onTriggerEnter(other);
        }


        public void OnTriggerExit2D(Collider2D other)
        {
            _currentState.onTriggerExit(other);
        }

        #endregion


        #region faceStuff

        /// <summary>
        /// Fruit will rotate eyeballs to follow the given target every frame, until target is removed or overriden.  if null is passed, eyeball will stay frozen in default prefab position.
        /// </summary>
        /// <param name="target"> target that eyeballs will follow. set null to keep eye position frozen in default position</param>
        public void LockEyeTrack(Transform target = null, Vector3? offset = null)
        {
            if (offset == null)
                offset = Vector3.zero;

            foreach (var eye in eyes)
            {
                eye.target = target;
                eye.offset = (Vector3) offset;
            }
        }

        /// <summary>
        /// Fruit will rotate eyeballs to follow given position for only one frame.  Will freeze in last position until new pos or target is given.  
        /// </summary>
        /// <param name="pos">Vector3</param>
        public void EyeTrackOneShot(Vector3 pos)
        {
            foreach (var eye in eyes)
            {
                eye.target = null;
                eye.TrackOneFrame(pos);
            }
        }

        /// <summary>
        /// Change the mouth expression of the fruit, like smiling, suprise, none, etc.  Expressions defined in Mouth.cs.
        /// </summary>
        /// <param name="sprite"></param>
        public void SetMouth(MouthSprite sprite) => mouth.SetSprite(sprite);


        public void TriggerVFX()
        {
            if (HitVFX && vfxSpawnpoint)
            {
                Instantiate(HitVFX, vfxSpawnpoint.position, transform.rotation);
            }

        }


        #endregion

        #region TreeInteractions


        public bool IsAttached => (_attachPoint != null);

        public void AttachTo(AttachPoint point, bool snap = false)
        {
            _attachPoint = point;
            if (snap)
            {
                transform.position = _attachPoint.transform.position;
            }
            else
            {
                StartCoroutine(AttachLerp());
            }

            _attachPoint.Add(this);
        }

        IEnumerator AttachLerp()
        {
            Vector3 target = _attachPoint.transform.position;
            while (Vector3.Distance(transform.position, _attachPoint.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, 0.15f);
                yield return null;
            }
            transform.position = _attachPoint.transform.position;
        }

        public void Detach()
        {
            if (_attachPoint)
                _attachPoint.Remove(this);

            _attachPoint = null;
        }

        /// <summary>
        /// used while coroutines are running, preventing player from accidentally interacting with fruits and changing their states before transitioning is finished.
        /// Will reset to false after switching currentState.
        // </summary>
        public void DisableInputDuringCurrentState()
        {
            _disabledInputs = true;
        }

        #endregion

        public void OnDestroy()
        {
            _currentState = null;
            //FruitManager.Instance.Remove(this);
        }
    }
}
