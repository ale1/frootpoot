﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using FrootPoot.Utils;
using UnityEditor.Experimental;
using Random = UnityEngine.Random;


namespace FrootPoot
{

    public class FruitManager : MonoBehaviour
    {
        #region singleton

        private static FruitManager _instance;

        public static FruitManager Instance
        {
            get { return _instance; }
        }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        #endregion

        private float timer;

        private List<Fruit> Fruits = new List<Fruit>();
        [SerializeField] private Tree tree;
        [SerializeField] private GameObject[] FruitPrefabs;

        private bool finishedLoading = false;


        #region Monobehaviour

        void Start()
        {
#if UNITY_EDITOR
            if (tree == null)
                Debug.LogError("potato head, did you forget to setup a ref?");
#endif

            StartCoroutine(SpawnInitialFruits());
        }

        void Update()
        {
            if (!finishedLoading) return;


        }

        #endregion

        IEnumerator SpawnInitialFruits()
        {
            tree.Setup();

            List<Bush> bushes = tree.Bushes;
            bushes.Shuffle(); //randomize order of bushes so every playthrough loads a bit different

            yield return new WaitForSeconds(1f); //dramatic pause.

            int index = 0;
            foreach (var bush in bushes)
            {
                List<AttachPoint> attachPoints = bush.AttachPoints;
                attachPoints.Shuffle();

                foreach (var attachPoint in attachPoints)
                {
                    GameObject fruitGO = Instantiate(FruitPrefabs[index], this.transform);
                    index = (index == FruitPrefabs.Length - 1 ? 0 : index + 1);
                    Fruit fruit = fruitGO.GetComponent<Fruit>();
                    Fruits.Add(fruit);
                    fruit.parentTree = tree;
                    fruit.transform.position = attachPoint.transform.position;
                    fruit.SetState(new FruitStateAttached(fruit, attachPoint));

                    yield return new WaitForSeconds(0.5f);
                }

                bush.EscapeAllowed = true;
            }

            yield return null;

            finishedLoading = true;
        }


        /// <summary>
        /// called when Fruit is being destroyed
        /// </summary>
        /// <param name="fruit"></param>
        public void Remove(Fruit fruit)
        {
            Fruits.Remove(fruit);
            SpawnFruit(); //spawn new one to remplaze the one just destroyed
        }

        private void SpawnFruit()
        {
            int fruitIndex = Random.Range(0, FruitPrefabs.Length);
            GameObject fruitGO = Instantiate(FruitPrefabs[fruitIndex], this.transform);
            Fruit fruit = fruitGO.GetComponent<Fruit>();
            Fruits.Add(fruit);
            fruit.parentTree = tree;
            Transform[] bounds = {tree.leftBound, tree.rightBound};
            int index = Random.Range(0, bounds.Length);
            float y = Random.Range(tree.lowerBound.position.y, tree.upperBound.position.y);
            fruit.transform.position = new Vector3(bounds[index].position.x, y, y * 10); // todo: dont use magicNumber
            fruit.SetState(new FruitStateWalking(fruit));
        }

    }

}
