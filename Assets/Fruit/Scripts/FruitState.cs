﻿
using UnityEngine;

namespace FrootPoot
{
    public abstract class FruitState
    {
        protected Fruit Fruit;
        protected bool Transitioning = false;

        public bool InteractionDisabled = false;

        public FruitState(Fruit fruit)
        {
            this.Fruit = fruit;
        }

        //Base StateMachine behaviours
        public abstract void OnUpdate();
        public abstract void OnStateEnter();
        public abstract void OnStateExit();

        //Pointer Events
        public abstract void onMouseDown();
        public abstract void onMouseUp();
        public abstract void onMouseEnter();
        public abstract void onMouseExit();

        public abstract void onTriggerEnter(Collider2D other);
        public abstract void onTriggerExit(Collider2D other);


    }

}