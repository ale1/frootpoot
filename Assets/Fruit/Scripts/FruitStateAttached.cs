﻿
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace FrootPoot
{
    public class FruitStateAttached : FruitState
    {
        private AttachPoint attachPoint;

        //constructor
        public FruitStateAttached(Fruit fruit, AttachPoint attachPoint) : base(fruit)
        {
            this.attachPoint = attachPoint;
        }

        public override void OnStateEnter()
        {
            Fruit.AttachTo(attachPoint);
            AudioClip clip = Resources.Load<AudioClip>("Audio/hello");
            Fruit.audio.PlayOneShot(clip);
            Fruit.SetMouth(Mouth.MouthSprite.NONE);
            Transitioning = false;

        }

        public override void OnStateExit()
        {
            Transitioning = true;
        }

        public override void OnUpdate()
        {
            if (!Transitioning)
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Fruit.EyeTrackOneShot(pos);

                Fruit.transform.position = attachPoint.transform.position;
            }

        }


        #region PointerHandlers

        public override void onMouseDown()
        {
            Fruit.Detach();
            Fruit.SetState(new FruitStateDragging(Fruit));
        }

        public override void onMouseUp()
        {
            // do nothing
        }

        public override void onMouseEnter()
        {
            //show open mouth
            Fruit.SetMouth(Mouth.MouthSprite.NONE);
        }

        public override void onMouseExit()
        {

        }

        public override void onTriggerEnter(Collider2D other)
        {

        }

        public override void onTriggerExit(Collider2D other)
        {

        }

        #endregion

    }
}
