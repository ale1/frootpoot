﻿
using UnityEngine;

namespace FrootPoot
{
    public class FruitStateDragging : FruitState
    {

        //state specific vars
        private Camera _cam;
        private AttachPoint _collidingAttachPoint = null;


        public FruitStateDragging(Fruit fruit) : base(fruit)
        {

        }

        public override void OnStateEnter()
        {
            _cam = Camera.main;
            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Transitioning = true;
        }

        public override void OnUpdate()
        {

            Vector3 targetPos = _cam.ScreenToWorldPoint(Input.mousePosition);
            Fruit.transform.position = new Vector3(targetPos.x, targetPos.y, Fruit.transform.position.z);
        }

        #region mouseHandlers

        public override void onMouseDown()
        {
            //ignore. fruit is not interactable until it lands.
        }

        public override void onMouseUp() //player has released fruit
        {
            if (_collidingAttachPoint != null && _collidingAttachPoint.IsFree)
            {
                Fruit.SetState(new FruitStateAttached(Fruit, _collidingAttachPoint));
            }
            else
            {
                Fruit.SetState(new FruitStateFalling(Fruit));
            }
        }

        public override void onMouseEnter()
        {

        }

        public override void onMouseExit()
        {

        }

        public override void onTriggerEnter(Collider2D other)
        {
            if (other.transform.CompareTag("AttachPoint"))
            {
                AttachPoint ap = other.GetComponent<AttachPoint>();
                if (ap.IsFree)
                    _collidingAttachPoint = ap;
            }
        }

        public override void onTriggerExit(Collider2D other)
        {
            if (other.transform.CompareTag("AttachPoint"))
            {
                if (_collidingAttachPoint == other.transform.GetComponent<AttachPoint>())
                {
                    _collidingAttachPoint = null;
                }
            }
        }

        #endregion

    }

}
