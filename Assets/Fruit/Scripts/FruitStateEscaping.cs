﻿using UnityEngine;
using static Mouth; // syntactic sugar

namespace FrootPoot
{
    public class FruitStateEscaping : FruitState
    {
        //state-specific vars
        private AttachPoint _attachPoint;
        private Vector3 originalScale;

        public FruitStateEscaping(Fruit fruit, AttachPoint attachPoint) : base(fruit) //constructor
        {
            this._attachPoint = attachPoint;
        }

        public override void OnStateEnter()
        {
            Fruit.Detach(); // detach from bush 
            Fruit.AttachTo(_attachPoint, true); // attach to rocket
            originalScale = Fruit.transform.localScale;
            Fruit.transform.localScale = originalScale * 0.7f; //make fruit somewhat smaller while inside vehciles.
            //Fruit.Animator.... //todo: some animation 
            //Fruit.SetMouth(MouthSprite.FLAT); // todo: some mouth effect
            AudioClip clip = Resources.Load<AudioClip>("Audio/byebye");
            Fruit.audio.PlayOneShot(clip);
            Fruit.LockEyeTrack(_attachPoint.transform, new Vector3(0, 10f, 0)); //look up;
            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Fruit.transform.localScale = originalScale; //put fruit back to original scale when leaving state
            Transitioning = true;
        }

        public override void OnUpdate()
        {
            if (!Transitioning)
            {
                if (_attachPoint)
                    Fruit.transform.position = _attachPoint.transform.position;
            }
        }


        #region MouseHandling
        public override void onMouseDown() { /* do nothing. not interactable while escaping.*/}

        public override void onMouseUp() { }

        public override void onMouseEnter() { }

        public override void onMouseExit() { }

        #endregion

        #region CollisionDetection
        public override void onTriggerEnter(Collider2D other) { }

        public override void onTriggerExit(Collider2D other) { }
        #endregion
    }
}
