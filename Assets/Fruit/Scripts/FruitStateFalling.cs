﻿using UnityEngine;

namespace FrootPoot
{


    public class FruitStateFalling : FruitState
    {
        private static readonly int Falling = Animator.StringToHash("falling");

        public FruitStateFalling(Fruit fruit) : base(fruit)
        {
        } //contructor

        public override void OnStateEnter()
        {
            Fruit.Detach();
            Fruit.SetMouth(Mouth.MouthSprite.SUPRISE);
            Fruit.Animator.SetBool(Falling, true);
            AudioClip clip = Resources.Load<AudioClip>("Audio/caramba");
            Fruit.audio.PlayOneShot(clip);
            Fruit.physics.Activate();
            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Transitioning = true;
            Fruit.physics.Deactivate();

        }

        public override void OnUpdate()
        {
            if (!Transitioning)
            {
                if (Fruit.physics.Falling == false)
                    Fruit.SetState(new FruitStateWalking(Fruit));
            }
        }
        #region MouseHandling
        public override void onMouseDown() { }

        public override void onMouseUp() { }

        public override void onMouseEnter() { }

        public override void onMouseExit() { }
        #endregion


        #region CollisionHandling
        public override void onTriggerEnter(Collider2D other)
        {

        }

        public override void onTriggerExit(Collider2D other)
        {

        }

        #endregion



    }

}
