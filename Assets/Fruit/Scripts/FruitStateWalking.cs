﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using static Mouth;
using Random = UnityEngine.Random; // syntactic sugar

namespace FrootPoot
{

    public class FruitStateWalking : FruitState
    {

        //state-specific vars
        private Vector3 walkingDest;
        private float maxWalkingDelta;

        public FruitStateWalking(Fruit fruit) : base(fruit)
        {
        } //contructor

        public override void OnStateEnter()
        {
            Fruit.TriggerVFX();
            maxWalkingDelta = Random.Range(0.4f, 0.6f); // this randomizes a bit the walking speed
            Fruit.Animator.SetBool("falling", false);
            Fruit.SetMouth(MouthSprite.FLAT);
            walkingDest = Fruit.parentTree.GetWalkingDestination();
            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Transitioning = true;
        }

        public override void OnUpdate()
        {
            if (!Transitioning)
            {
                Fruit.transform.position = Vector3.MoveTowards(Fruit.transform.position, walkingDest,
                    maxWalkingDelta * Time.deltaTime);
                Fruit.EyeTrackOneShot(walkingDest);
                if (Math.Abs(Fruit.transform.position.x - walkingDest.x) < 0.01f)
                {
                    //reached Destination, so switch to new destination
                    walkingDest = Fruit.parentTree.GetWalkingDestination(Fruit.transform.position);
                }
            }
        }

        public override void onMouseDown()
        {
            Fruit.SetState(new FruitStateDragging(Fruit));
        }

        public override void onMouseUp()
        {

        }

        public override void onMouseEnter()
        {

        }

        public override void onMouseExit()
        {

        }

        public override void onTriggerEnter(Collider2D other)
        {

        }

        public override void onTriggerExit(Collider2D other)
        {

        }
    }
}
