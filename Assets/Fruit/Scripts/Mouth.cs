﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Mouth : MonoBehaviour
{
    public enum MouthSprite
    {
        NONE,
        SUPRISE,
        FLAT
        
    }

    public Sprite SupriseMouth;
    public Sprite FlatMouth;
    
    private SpriteRenderer _rend;

    private void Awake()
    {
        _rend = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSprite(MouthSprite mouthSprite)
    {
        Sprite sprite;
        switch (mouthSprite)
        {
            case(MouthSprite.NONE):
                sprite = null;
                break;
            case(MouthSprite.SUPRISE):
                sprite = SupriseMouth;
                break;
            case(MouthSprite.FLAT):
                sprite = FlatMouth;
                break;
            default:
                sprite = null;
                break;
        }

        _rend.sprite = sprite;
        if(sprite)
            Debug.Log(sprite.name);
    }
}
