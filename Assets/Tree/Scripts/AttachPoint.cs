﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FrootPoot
{

    public class AttachPoint : MonoBehaviour
    {

        public bool IsFree => (AttachedFruit == null);

        public Fruit AttachedFruit { get; private set; }


        public Action OnFruitAttached;


        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Add(Fruit fruit)
        {
            if (AttachedFruit != null)
                Debug.LogError("oh no!, something went wrong here");
            else
            {
                AttachedFruit = fruit;
                OnFruitAttached?.Invoke();
            }
        }

        public void Remove(Fruit fruit)
        {
            if (AttachedFruit != fruit)
                Debug.LogError("ah caramba!, something went wrong here");
            else
            {
                AttachedFruit = null;
            }
        }
    }
}
