﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static FrootPoot.Fruit; //syntactic sugar


namespace FrootPoot
{


    [RequireComponent(typeof(Renderer))]
    public class Bush : MonoBehaviour
    {
        [HideInInspector]
        public bool EscapeAllowed; //must start in false and modified by script after setup is complete

        private SpriteRenderer _renderer;

        //Escape Vehicles
        public GameObject rocketPrefab;
        public GameObject balloonPrefab;
        public GameObject bananaCopterPrefab;
        private static readonly int CrazyRotation = Animator.StringToHash("CrazyRotation");


        public List<AttachPoint> AttachPoints { get; private set; }

        public List<Fruit> AttachedFruits
        {
            get
            {
                List<Fruit> fruits = new List<Fruit>();
                foreach (var ap in AttachPoints)
                {
                    fruits.Add(ap.AttachedFruit);
                }

                return fruits;
            }
        }

        // Start is called before the first frame update

        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void Setup()
        {
            _renderer = GetComponent<SpriteRenderer>();
            AttachPoints = GetComponentsInChildren<AttachPoint>().ToList();
            foreach (var ap in AttachPoints)
            {
                ap.OnFruitAttached += OnFruitAttachedHandler;
            }
        }

        /// <summary>
        /// called when a fruit has been attached to one of the child attach points
        /// </summary>
        private void OnFruitAttachedHandler()
        {
            //we want to check for condition where all attachPoints are full.
            foreach (var ap in AttachPoints)
            {
                if (ap.IsFree)
                    return;
            }

            bool sameFruitType = AttachedFruits.FindAll(x => x.flavor == AttachedFruits[0].flavor).Count ==
                                 AttachedFruits.Count;

            //all attachments are full, if the fruits get along they will escape together, otherwise fight and fall to the ground...

            if (EscapeAllowed && sameFruitType)
                StartCoroutine(StartFruitEscape(AttachedFruits));

            else
            {
                StartCoroutine(StartFruitFight());
            }


        }

        private IEnumerator StartFruitEscape(List<Fruit> AttachedFruits)
        {
            foreach (var fruit in AttachedFruits)
            {
                fruit.DisableInputDuringCurrentState();
            }

            yield return new WaitForSeconds(1.3f);

            GameObject vehicleGO;
            Vehicle vehicle;

            switch (AttachedFruits[0].flavor)
            {
                case (Flavor.APPLE):
                    vehicleGO = Instantiate(rocketPrefab);
                    break;
                case (Flavor.GRAPE):
                    vehicleGO = Instantiate(bananaCopterPrefab);
                    break;
                case (Flavor.MELON):
                    vehicleGO = Instantiate(rocketPrefab);
                    break;
                //todo balloon
                default:
                    vehicleGO = Instantiate(rocketPrefab);
                    break;
            }

            vehicle = vehicleGO.GetComponent<Vehicle>();
            vehicleGO.transform.position =
                this.transform.position + new Vector3(0, 0, 0.1f); //the extra z is to hide it behind the bush
            vehicle.Setup();
            int counter = 0;
            foreach (var fruit in AttachedFruits)
            {
                fruit.SetState(new FruitStateEscaping(fruit, vehicle.AttachPoints[counter++]));
            }

            vehicle.Move = true;
            StopAllCoroutines();
            StartCoroutine(FadeOut());

        }

        private IEnumerator StartFruitFight()
        {
            foreach (var fruit in AttachedFruits)
            {
                fruit.DisableInputDuringCurrentState();
            }

            yield return new WaitForSeconds(1.5f);

            Animator anim = this.GetComponent<Animator>(); //todo: cache this value.
            anim.SetTrigger(CrazyRotation);

            //get animation crazy rotation anim clip length
            float clipLength = 0f;
            string clipName = "CrazyRotation";
            AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;
            foreach (AnimationClip clip in clips)
            {
                if (clip.name == clipName)
                    clipLength = clip.length;
            }

            //safety check
            if (clipLength == 0)
            {
                clipLength = 2f;
                Debug.LogError("could not find animation clip with name:" + clipName);
            }

            yield return new WaitForSeconds(clipLength);

            foreach (var fruit in AttachedFruits)
            {
                fruit.SetState(new FruitStateFalling(fruit));
            }

        }

        //Todo: might be better to do fades in and out through animation.

        #region bushEffects

        IEnumerator FadeOut()
        {
            float alpha = _renderer.color.a;

            while (alpha > 0.05f)
            {
                Color tmp = _renderer.color;
                alpha = Mathf.Lerp(tmp.a, 0, Time.deltaTime);
                tmp.a = alpha;
                _renderer.color = tmp;
                yield return null;
            }

            yield return new WaitForSeconds(0.6f);
            Color col = _renderer.color;
            col.a = 0;
            _renderer.color = col;
            StartCoroutine(FadeIn());
        }

        IEnumerator FadeIn()
        {
            float alpha = _renderer.color.a;

            while (alpha < 1f)
            {
                Color tmp = _renderer.color;
                alpha = Mathf.Lerp(tmp.a, 1, Time.deltaTime * 2f);
                tmp.a = alpha;
                _renderer.color = tmp;
                yield return null;
            }
            Color col = _renderer.color;
            col.a = 1;
            _renderer.color = col;
        }

        #endregion
    }
}
