﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FrootPoot
{


    public class Tree : MonoBehaviour
    {
        public List<Bush> Bushes { get; private set; }

        /// <summary>
        /// Maximum number of fruits allowed in the game.  is calculated based on available attach points on tree bushes.
        /// </summary>
        public int MaxFruits { get; private set; }

        #region Monobehaviours

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        #endregion


        public void Setup()
        {
            int MaxFruits = 0;
            Bushes = GetComponentsInChildren<Bush>().ToList();
            foreach (var bush in Bushes)
            {
                bush.Setup();
                MaxFruits += bush.AttachPoints.Count;
            }

        }


        #region movementBoundries

        public Transform upperBound;
        public Transform lowerBound;
        public Transform leftBound;
        public Transform rightBound;

        /// <summary>
        /// We dont want the fruit always landing on the same "ground" or "horizon" line.  We use upper and lower bounds setup in the scene to randomize
        /// the y-value of where the fruit will come to a stop when falling. 
        /// </summary>
        /// <returns>randomized y vector position of where to land the fruit</returns>
        public float GetDropStopLocation()
        {
            float y = Random.Range(lowerBound.transform.position.y, upperBound.transform.position.y);
            return y;
        }


        /// <summary>
        /// Once a fruit has landed, it will randomly walk to one of the lateral (let/right) bounds off-camera.  On arriving, it will get a new destiantion on the opposite lateral bound.
        /// </summary>
        public Vector3 GetWalkingDestination(Vector3? currentPos = null)
        {
            Transform trf;

            if (currentPos != null)
            {
                Vector3 pos = (Vector3) currentPos;
                trf = Vector3.Distance(pos, leftBound.position) < Vector3.Distance(pos, rightBound.position)
                    ? rightBound
                    : leftBound;
            }
            else
            {
                Transform[] sideBounds = {leftBound, rightBound};
                int index = Random.Range(0, sideBounds.Length);
                trf = sideBounds[index];
            }
            
            float targetX = trf.position.x;
            float targetY = Random.Range(lowerBound.position.y, upperBound.position.y);
            float targetZ = targetY * 10;

            return new Vector3(targetX, targetY, targetZ);
        }

        #endregion
    }
}
