﻿
using UnityEngine;

    public class ParticleDestroyer : MonoBehaviour
    {
        // Start is called before the first frame update

        void Start()
        {
            Destroy(this.gameObject, GetComponent<ParticleSystem>().main.duration);
        }


        // Update is called once per frame
        void Update()
        {

        }
    }

