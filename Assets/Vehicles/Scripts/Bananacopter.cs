﻿
using UnityEngine;

namespace FrootPoot
{


    public class Bananacopter : Vehicle
    {
        // Start is called before the first frame update
        void Start()
        {
            Destroy(this.gameObject, 5f);
        }

        // Update is called once per frame
        void Update()
        {
            _speed += Time.deltaTime;
            var trf = transform;
            trf.position += trf.up * (_speed * Time.deltaTime);
        }

        private void OnDestroy()
        {
            foreach (var ap in AttachPoints)
            {
                FruitManager.Instance.Remove(ap.AttachedFruit);
            }
        }
    }
}
