﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace FrootPoot
{

    public class Rocket : Vehicle
    {
        
        // Start is called before the first frame update
        void Start()
        {
            Destroy(this.gameObject, 6f);
        }

        // Update is called once per frame
        void Update()
        {
            if (!Move) return;
            _speed += Time.deltaTime;
            var trf = transform;
            trf.position += trf.up * (_speed * Time.deltaTime);
        }

        private void OnDestroy()
        {
            foreach (var ap in AttachPoints)
            {
                FruitManager.Instance.Remove(ap.AttachedFruit);
            }
        }
    }
}
