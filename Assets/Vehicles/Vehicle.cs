﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace FrootPoot
{
    public class Vehicle : MonoBehaviour
    {
        [HideInInspector] public bool Move;
        public List<AttachPoint> AttachPoints { get; protected set; }
        protected float _speed;

        public void Setup()
        {
            AttachPoints = GetComponentsInChildren<AttachPoint>().ToList();
            Move = false;
        }
    }
}
